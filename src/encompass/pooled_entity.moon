current_folder = (...)\gsub('%.[^%.]+$', '') .. '.'

Entity = require(current_folder .. 'entity')

class PooledEntity extends Entity
    new: (id, world, spawner) =>
        super id, world
        @spawner = spawner
        @world\__mark_entity_for_deactivation @

    deactivate: =>
        if @active
            @world\__mark_entity_for_deactivation @
            @spawner\__deactivate @

    destroy: =>
        @deactivate!

return PooledEntity