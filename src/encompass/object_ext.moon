getters = (klass, getter_funcs) ->
    klass.__base.__index = (key) =>
        if getter = getter_funcs[key]
          getter @
        else
            klass.__base[key]

setters = (klass, setter_funcs) ->
    klass.__base.__newindex = (key, val) =>
        if setter = setter_funcs[key]
            setter @, val
        else
            rawset @, key, val

{ :getters, :setters }