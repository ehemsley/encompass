inherited = (superklass, subklass) ->
    for k, v in pairs superklass.required_field_types
        if subklass.required_field_types[k] == nil then
            subklass.required_field_types[k] = v

    for k, v in pairs superklass.optional_field_types
        if subklass.optional_field_types[k] == nil then
            subklass.optional_field_types[k] = v

    for k, v in pairs subklass.required_field_types
        subklass.field_types[k] = v

    for k, v in pairs subklass.optional_field_types
        assert subklass.required_field_types[k] == nil, "Field '" .. k .. "' on " .. subklass.__name .. " cannot be both required and optional"
        subklass.field_types[k] = v

    subklass.__base.__tostring = superklass.__tostring

define = (superklass, name, required_field_types = {}, optional_field_types = {}) ->
    return class extends superklass
        @__name: name
        @required_field_types: required_field_types
        @optional_field_types: optional_field_types
        @field_types: {}
        @define: define
        @__inherited: inherited

class Component
    @required_field_types: {}
    @optional_field_types: {}
    @field_types: {}
    @define: define
    @__inherited: inherited

    __tostring: => "instance of " .. @@__name

    new: (entity) =>
        @__entity = entity
        @__active = true

    get_entity: =>
        @__entity

    subtype_of: (klass) =>
        return @@ == klass or (@@.__parent ~= nil and @@.__parent\subtype_of klass)

    activate: =>
        if not @__active then
            table.insert @__entity.world.entities_with_added_components, @__entity
            @on_activate!
            @__active = true

    deactivate: =>
        if @__active then
            table.insert @__entity.world.entities_with_removed_components, @__entity
            @on_deactivate!
            @__active = false

    on_initialize: ->
    on_deactivate: ->
    on_activate: ->
    on_destroy: ->

return Component