up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'
up_two_folders = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_two_folders .. 'utils')
Processor = require(up_one_folder .. 'processor')
SpawnMessage = require(up_two_folders .. 'messages.spawn')

class Spawner extends Processor
    @frozen: false

    @__inherited: (subklass) =>
        if not subklass.frozen then
            subklass\assert_has_message_type!
            assert subklass.message_type\subtype_of(SpawnMessage), subklass.__name .. " must consume a message of SpawnMessage type"
            assert subklass.spawn != nil and utils.callable(subklass.spawn), "spawn function must be overridden on " .. subklass.__name

    __update: =>
        for _, message in pairs @messages
            @spawn message

return Spawner