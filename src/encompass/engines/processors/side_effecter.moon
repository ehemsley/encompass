up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'
up_two_folders = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_two_folders .. 'utils')
Processor = require(up_one_folder .. 'processor')
SideEffectMessage = require(up_two_folders .. 'messages.side_effect')

class SideEffecter extends Processor
    @__inherited: (subklass) =>
        subklass\assert_has_message_type!
        assert subklass.message_type\subtype_of(SideEffectMessage), subklass.__name .. " must consume a message of SideEffectMessage type"
        assert subklass.effect != nil and utils.callable(subklass.effect), "effect function must be overridden on " .. subklass.__name

    __update: (dt) =>
        for _, message in pairs @messages
            @effect message, dt

return SideEffecter