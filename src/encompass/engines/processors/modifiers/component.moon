up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'
up_three_folders = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_three_folders .. 'utils')
ComponentMessage = require(up_three_folders .. 'messages.component')
Modifier = require(up_one_folder .. 'modifier')

class ComponentModifier extends Modifier
    @__inherited: (subklass) =>
        subklass\assert_has_message_type!
        assert subklass.message_type\subtype_of(ComponentMessage), subklass.__name .. " must consume a message of ComponentMessage type"
        assert subklass.modify != nil and utils.callable(subklass.modify), subklass.__name .. " must implement modify function"

    new: (world, ...) =>
        @component_to_message = {}
        super world, ...

    __update: (dt) =>
        for component, message in pairs @component_to_message
            @modify component, @__create_frozen_fields(component), message, dt

    __track_message: (message) =>
        assert @component_to_message[message.component] == nil, @@__name .. " caught multiple messages for the same component, extend MultiMessageComponentModifier instead"
        @component_to_message[message.component] = message

    __untrack_message: (message) =>
        @component_to_message[message.component] = nil

    __clean: =>
        utils.clear @component_to_message

return ComponentModifier