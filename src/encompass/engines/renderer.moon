up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_one_folder .. 'utils')
DrawComponent = require(up_one_folder .. 'draw_component')
Engine = require(up_one_folder .. 'engine')

class Renderer extends Engine
    @__inherited: (subklass) =>
        subklass\assert_component_types!
        assert subklass.render != nil and utils.callable(subklass.render), "render must be overridden on " .. subklass.__name

        draw_component_count = 0
        for _, component_type in pairs subklass.component_types
            if component_type\subtype_of(DrawComponent) then draw_component_count += 1

        assert draw_component_count == 1, "Renderer " .. subklass.__name .. " must track exactly one DrawComponent, was given " .. draw_component_count

return Renderer