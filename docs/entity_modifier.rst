.. _entity_modifier:

EntityModifier
==============

::

    local EntityModifier = require('encompass').EntityModifier

An EntityModifier is an Engine which is responsible
for consuming :ref:`EntityMessages <entity_message>`
and manipulating :ref:`Entities <entity>` in response,
for example, adding a component to an entity
or deactivating an entity.

EntityModifiers are defined by the EntityMessage prototype they track
and the ``modify`` function they implement.

It is an anti-pattern to read Entities or Components inside an EntityModifier.
*Do not do this.*

Function Reference
------------------

.. function:: EntityModifier.define(name, message_type, state_message_types)

    :param string name: The name of the EntityModifier.
    :param prototype message_type: An EntityMessage prototype that should be tracked by the EntityModifier.
    :param table state_message_types: An array-style table of StateMessage types that should be tracked by the EntityModifier. *Optional*

Defines an EntityModifier that will track the given EntityMessage prototype,
and optionally track the given StateMessage types.

.. function:: EntityModifier:get_state_message(state_message_type)

    :param StateMessage state_message_type: A StateMessage prototype that has been tracked by the EntityModifier.
    :returns: ``StateMessage`` or ``nil`` An instantiated StateMessage of the given typrototypepe, or nil if none has been produced this frame.

.. function:: EntityModifier:modify(entity, messages, dt)

    :param Entity entity: A reference to an entity referenced by the tracked Message.
    :param table messages:
        An array-style table containing references to multiple messages
        that have been tracked by the EntityModifier and all reference the same component.
    :param dt:
        The delta time given by the World's current frame update.

This callback is triggered when one or more EntityMessages of the specified prototype is produced by a Detecter.
The programmer must override this callback or an error will be thrown.

Example
-------

::

    local AddComponentMessage = require('game.messages.add_component')

    local EntityModifier = require('encompass').EntityModifier
    local AddComponentModifier = EntityModifier.define('AddComponentModifier', AddComponentMessage)

    function AddComponentModifier:modify(entity, messages, dt)
        for _, message in pairs(messages) do
            entity:add_component(message.component_to_add, message.component_args)
        end
    end

    return AddComponentModifier