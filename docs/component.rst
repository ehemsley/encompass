.. _component:

Component
=========

::

    local Component = require("encompass").Component

A Component can be thought of as a collection of data
which can be attached to an entity.

Component types are defined by their required field types
and optional field types,
which are used to instantiate the Components themselves.
See the :ref:`Field Type Reference <field_type_reference>`
for a list of valid field types.

Components are `active` by default and can be deactivated.
While inactive, Entities containing the Component will not
be tracked by Detectors that track the Component prototype.
This is useful for situations where you may want
to temporarily disable a component without destroying
information. Note that the activation status of a Component
does not affect the activation status of its Entity.

Components do not contain any logic,
but they do have optional callbacks to handle any side-effects,
for example, creating or destroying bodies in a physics world.

Defining non-side-effect logic on a Component is an anti-pattern.
*Do not do this.*

Function Reference
------------------

.. function:: Component.define(name, required_field_types, optional_field_types)

    :param string name: The name of the Component prototype.
    :param table required_field_types:
        A table where the keys are field names and the values are types.
        If one of these fields is missing when the Component is instantiated,
        or a field is passed to the component with an incorrect type, an error is thrown.
    :param table optional_field_types:
        A table where the keys are field names and the values are types.
        If a field is passed to the component with an incorrect type, an error is thrown.
    :returns: A new Component prototype.

**Example**::

    local Component = require("encompass").Component
    local PositionComponent = Component.define('PositionComponent', {
        x = 'number',
        y = 'number'
    })

    return PositionComponent

.. function:: Component:activate()

Activates the Component. Registers attached Entity with appropriate Detectors.
Does nothing if the Component is already active.

.. function:: Component:deactivate()

Deactivates the Component. Unregisters attached Entity with appropriate Detectors.
Does nothing if the Component is already inactive.

Callback Reference
------------------

``Component:initialize()``
    Runs when the Component is added to an Entity.

``Component:on_destroy()``
    Runs when the Component is removed from an Entity,
    or the attached Entity is destroyed.

``Component:on_activate()``
    Runs when the attached Entity is activated.

``Component:on_deactivate()``
    Runs when the attached Entity is deactivated.
