**encompass** - A Hyper ECS framework for Lua
=============================================

**encompass** is a powerful engine-agnostic Hyper ECS framework
ideal for game development.

..  toctree::
    :maxdepth: 1

    Getting Started <getting_started>
    API Reference <api>
    About Hyper ECS <hyper-ecs>
    Acknowledgements <acknowledgements>

**NOTE:** This project is licensed under the Cooperative Software License.
You should have received a copy of this license with the
framework code. If not, please see LICENSE_.

.. _LICENSE: https://gitlab.com/ehemsley/encompass/blob/master/LICENSE

The long and short of it is that if you are working on behalf of a corporation
and not for yourself as an individual or a cooperative,
you are *not* welcome to use this software as-is.
If this is the case, please contact <evan@moonside.games>
to negotiate an appropriate licensing agreement.