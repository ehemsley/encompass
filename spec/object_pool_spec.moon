ObjectPool = require("encompass.object_pool")

helper = require('spec.helper')
helper.register(assert)

describe "table pool", ->
    class MyClass

    describe "constructor", ->
        describe "no args", ->
            pool = ObjectPool!

            it "generates 16 objects", ->
                assert.are.equal 16, pool.size

            it "puts 16 objects in the inactive object table", ->
                count = 0
                for _, _ in pairs pool.inactive_objects
                    count += 1

                assert.are.equal 16, count

            it "puts zero objects in the active object table", ->
                count = 0
                for _, _ in pairs pool.active_objects
                    count += 1

                assert.are.equal 0, count

            it "creates empty tables", ->
                assert.are.equal "table", type(next(pool.inactive_objects))
                assert.is_empty next(pool.inactive_objects)

        describe "size passed in", ->
            pool = ObjectPool 32

            it "generates n objects", ->
                assert.are.equal 32, pool.size

            it "puts 16 objects in the inactive object table", ->
                count = 0
                for _, _ in pairs pool.inactive_objects
                    count += 1

                assert.are.equal 32, count

            it "puts zero objects in the active object table", ->
                count = 0
                for _, _ in pairs pool.active_objects
                    count += 1

                assert.are.equal 0, count

            it "creates empty tables", ->
                assert.are.equal "table", type(next(pool.inactive_objects))
                assert.is_empty next(pool.inactive_objects)

        describe "size and generator function passed in", ->
            pool = ObjectPool 32, () -> MyClass!

            it "generates n objects", ->
                assert.are.equal 32, pool.size

            it "creates objects from the generator", ->
                assert.are.same MyClass, next(pool.inactive_objects).__class

    describe "obtain", ->
        describe "inactive objects exist", ->
            pool = ObjectPool 32, () -> MyClass!
            instance = pool\obtain!

            it "returns an object", ->
                assert.are.same MyClass, instance.__class

            it "puts the object in the active pool", ->
                assert.has_value pool.active_objects, instance

            it "removes the object from the inactive pool", ->
                assert.is_not.has_value pool.inactive_objects, instance

        describe "no more inactive objects remain", ->
            describe "and has expand behavior", ->
                pool = ObjectPool 32, () -> MyClass!

                for _ = 1, 32
                    pool\obtain!

                instance = pool\obtain!

                it "doubles the pool size", ->
                    assert.are.equal 64, pool.size

                it "generates the correct amount of inactive objects", ->
                    count = 0
                    for _, _ in pairs pool.inactive_objects
                        count += 1

                    assert.are.equal 31, count

                it "puts the object in the active pool", ->
                    assert.has_value pool.active_objects, instance

                it "removes the object from the inactive pool", ->
                    assert.is_not.has_value pool.inactive_objects, instance

            describe "and has custom behavior", ->
                test_value = false
                throw = -> test_value = true

                pool = ObjectPool 32, (() -> MyClass!), throw

                for _ = 1, 32
                    pool\obtain!

                pool\obtain!

                it "calls the throw function", ->
                    assert.true test_value

            describe "and has fallible behavior", ->
                pool = ObjectPool 32, (() -> MyClass!), ObjectPool.OverflowBehaviors.fallible

                for _ = 1, 32
                    pool\obtain!

                instance = pool\obtain!

                it "does nothing", ->
                    assert.are.equal nil, instance

    describe "free", ->
        pool = ObjectPool 32, () -> MyClass!

        describe "the object is from the pool", ->
            instance = pool\obtain!
            pool\free instance

            it "returns the object to the inactive pool", ->
                assert.has_value pool.inactive_objects, instance

            it "removes the object from the active pool", ->
                assert.is_not.has_value pool.active_objects, instance

        describe "the object is not from the pool", ->
            instance = MyClass!

            it "throws an error", ->
                assert.has_error () -> pool\free instance, "cannot free inactive object"