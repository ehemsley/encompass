World = require('encompass.world')
Component = require('encompass.component')
Detecter = require('encompass.engines.detecter')

helper = require('spec.helper')
helper.register(assert)

describe "Component", ->
    describe "define", ->
        TestComponent = Component\define "TestComponent", { a: "number" }, { o: "string" }

        it "defines the component", ->
            assert.are.same "TestComponent", TestComponent.__name
            assert.are.same "number", TestComponent.field_types.a
            assert.are.same "string", TestComponent.field_types.o

    describe "on initialization", ->
        it "calls the on_initialize callback", ->
            world = World!
            entity = world\create_entity!

            class InitializerComponent extends Component
                on_initialize: => @thing = 5

            component = entity\add_component InitializerComponent
            assert.are.equal(5, component.thing)

    describe "on define", ->
        class NewComponent extends Component
            @required_field_types: { x: "number", y: "number" }
            @optional_field_types: { tag: "string" }

        it "sets field types correctly", ->
            assert.are.same NewComponent.field_types.x, "number"
            assert.are.same NewComponent.field_types.y, "number"
            assert.are.same NewComponent.field_types.tag, "string"

    describe "on inherit", ->
        class ParentComponent extends Component
            @required_field_types: { a: "number", b: "string" }
            @optional_field_types: { o: "number" }

        describe "when field types are valid", ->
            class InheritedComponent extends ParentComponent
                @required_field_types: { a: "string", c: "number" }

            it "defines required field types", ->
                assert.are.same InheritedComponent.field_types.a, "string"
                assert.are.same InheritedComponent.field_types.b, "string"
                assert.are.same InheritedComponent.field_types.c, "number"
                assert.are.same InheritedComponent.field_types.o, "number"

        describe "when field types are invalid", ->
            func = ->
                class InheritedComponent extends ParentComponent
                    @optional_field_types: { a: "number" }

            it "throws an error", ->
                assert.error func, "Field 'a' on InheritedComponent cannot be both required and optional"

    describe "activate", ->
        describe "is inactive", ->
            test_activate_value = 0

            class TestComponent extends Component
                on_activate: -> test_activate_value += 1

            class TestDetecter extends Detecter
                @component_types: { TestComponent }
                detect: ->

            world = World!
            test_detecter = world\add_detecter TestDetecter
            entity = world\create_entity!
            test_component = entity\add_component TestComponent

            test_component\deactivate!
            world\update 0.01
            test_component\activate!
            world\update 0.01

            it "calls the on_activate callback", ->
                assert.are.equal 1, test_activate_value

            it "re-registers entity with appropriate detecters", ->
                assert.has_value entity.tracked_by, test_detecter

    describe "deactivate", ->
        describe "is active", ->
            test_deactivate_value = 0

            class TestComponent extends Component
                on_deactivate: -> test_deactivate_value += 1

            class TestDetecter extends Detecter
                @component_types: { TestComponent }
                detect: ->

            world = World!
            test_detecter = world\add_detecter TestDetecter
            entity = world\create_entity!
            test_component = entity\add_component TestComponent

            test_component\deactivate!
            world\update 0.01

            it "calls the deactivate callback", ->
                assert.are.equal 1, test_deactivate_value

            it "unregisters entity with detecters", ->
                assert.not.has_value entity.tracked_by, test_detecter
