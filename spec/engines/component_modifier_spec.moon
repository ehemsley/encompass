World = require('encompass.world')
Component = require('encompass.component')
ComponentMessage = require('encompass.messages.component')
ComponentModifier = require('encompass.engines.processors.modifiers.component')
StateMessage = require('encompass.messages.state')

utils = require('encompass.utils')

helper = require('spec.helper')
helper.register(assert)

describe "ComponentModifier", ->
    ComponentA = Component\define "ComponentA", a: 'number'
    ComponentB = Component\define "ComponentB", b: 'number'
    ComponentC = Component\define "ComponentC", c: 'number'

    MessageA = ComponentMessage\define "MessageA", component: ComponentA
    MessageB = ComponentMessage\define "MessageB", component: ComponentB
    MessageC = ComponentMessage\define "MessageC", component: ComponentC

    describe "when a message is created", ->
        class TestModifier extends ComponentModifier
            @message_type: MessageA
            modify: =>

        world = World!
        modifier = world\add_modifier TestModifier

        entity = world\create_entity!
        a_component = entity\add_component ComponentA, "a", 4
        c_component = entity\add_component ComponentC, "c", 3

        it "tracks the message if correct type", ->
            a_message = world\create_message MessageA, 'component', a_component
            world\__check_messages!
            assert.are.same modifier.component_to_message[a_component], a_message
            modifier\__clean!

        it "does not track the message if not correct type", ->
            world\create_message MessageC, "component", c_component
            world\__check_messages!
            assert.nil modifier.component_to_message[c_component]
            modifier\__clean!

    describe "create_entity", ->
        world = World!

        class TestModifier extends ComponentModifier
            @message_type: MessageB
            modify: =>

        test_modifier = world\add_modifier TestModifier

        it "creates entity and adds it to world", ->
            entity = test_modifier\create_entity!
            assert.has_value world.entities, entity

    describe "modify", ->
        TestComponent = Component\define "TestComponent", a: "number"
        TestMessage = ComponentMessage\define "TestMessage", component: TestComponent

        local test_component, test_message

        class TestComponentModifier extends ComponentModifier
            @message_type: TestMessage

            modify: (component, frozen_fields, message, dt) =>
                assert.are.same test_component, component
                assert.are.equal 3, frozen_fields.a
                assert.are.same test_message, message
                assert.are.equal 0.01, dt

        world = World!

        world\add_modifier TestComponentModifier

        entity = world\create_entity!
        test_component = entity\add_component TestComponent, "a", 3
        test_message = world\create_message TestMessage, "component", test_component

        it "is called with correct arguments", ->
            world\__check_messages!
            world\__modify 0.01

    describe "state components", ->
        TestMessage = ComponentMessage\define "TestMessage"
        TestStateMessageA = StateMessage\define "TestStateMessageA", value: "number"
        TestStateMessageB = StateMessage\define "TestStateMessageB", value: "number"

        class TestStateMessageComponentModifier extends ComponentModifier
            @message_type: TestMessage
            @state_message_types: { TestStateMessageA, TestStateMessageB }

            modify: =>

        world = World!
        modifier = world\add_modifier TestStateMessageComponentModifier

        it "is stored on the processor", ->
            test_state_message_a = world\create_message TestStateMessageA, "value", 2
            test_state_message_b = world\create_message TestStateMessageB, "value", 3
            world\__check_messages!
            world\__check_state_messages!
            assert.are.same test_state_message_a, modifier\get_state_message(TestStateMessageA)
            assert.are.same test_state_message_b, modifier\get_state_message(TestStateMessageB)
            assert.are.equal 2, test_state_message_a.value
            assert.are.equal 3, test_state_message_b.value

        it "is cleared after world processes messages", ->
            test_state_message_a = world\create_message TestStateMessageA, "value", 2
            test_state_message_b = world\create_message TestStateMessageB, "value", 3
            world\__modify 0.01
            assert.is_not.has_value modifier.state_components, test_state_message_a
            assert.is_not.has_value modifier.state_components, test_state_message_b

    describe "multiple messages", ->
        TestComponent = Component\define "TestComponent", a: "number"
        TestMessage = ComponentMessage\define "TestMessage", component: TestComponent

        class TestComponentModifier extends ComponentModifier
            @message_type: TestMessage

            modify: =>

        world = World!

        world\add_modifier TestComponentModifier

        entity = world\create_entity!
        test_component = entity\add_component TestComponent, "a", 3
        world\create_message TestMessage, "component", test_component
        world\create_message TestMessage, "component", test_component

        it "throws an error", ->
            func = ->
                world\__check_messages!
                world\__modify 0.01

            assert.has_error(func, "TestComponentModifier caught multiple messages for the same component, extend MultiMessageComponentModifier instead")

    describe "modify not overridden", ->
        TestComponent = Component\define "TestComponent"
        TestComponentMessage = ComponentMessage\define "TestComponentMessage", component: TestComponent

        func = ->
            class NotOverriddenModifier extends ComponentModifier
                @message_type: TestComponentMessage

        it "throws an error", ->
            assert.has_error func, "NotOverriddenModifier must implement modify function"

    describe "defined with no message type", ->
        func = ->
            class MyComponentModifier extends ComponentModifier
                modify: =>

        it "throws an error", ->
            assert.has_error func, "MyComponentModifier must have a message_type property"