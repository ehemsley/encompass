helper = require('spec.helper')
helper.register(assert)

World = require('encompass.world')
ComponentMessage = require('encompass.messages.component')
EntityMessage = require('encompass.messages.entity')
StateMessage = require('encompass.messages.state')
EntityModifier = require('encompass.engines.processors.modifiers.entity')

describe "EntityModifier", ->
    MessageA = EntityMessage\define "MessageA", a: "number"
    MessageC = EntityMessage\define "MessageC", c: "number"

    class TestEntityModifier extends EntityModifier
        @message_type: MessageA

        modify: =>

    describe "when a message is created", ->
        world = World!
        modifier = world\add_modifier TestEntityModifier

        test_entity = world\create_entity!

        describe "of correct type", ->
            a_message = world\create_message MessageA, "entity", test_entity, "a", 4
            world\__check_messages!

            it "tracks the message", ->
                assert.has_value modifier.entity_to_messages[test_entity], a_message
                modifier\__clean!

        describe "of incorrect type", ->
            c_message = world\create_message MessageC, "entity", test_entity, "c", 3
            world\__check_messages!

            it "does not track the message", ->
                assert.is_not.has_value modifier.entity_to_messages[test_entity], c_message
                modifier\__clean!

    describe "create_entity", ->
        world = World!

        test_modifier = world\add_modifier TestEntityModifier

        it "creates entity and adds it to world", ->
            test_entity = test_modifier\create_entity!
            assert.has_value world.entities, test_entity

    describe "modify", ->
        ModifyTestMessage = EntityMessage\define "ModifyTestMessage"

        local test_entity, test_message

        class ModifyTester extends EntityModifier
            @message_type: ModifyTestMessage

            modify: (entity, messages, dt) =>
                assert.are.same test_entity, entity
                assert.has_value messages, test_message
                assert.are.equal 0.01, dt

        world = World!

        world\add_modifier ModifyTester

        test_entity = world\create_entity!
        test_message = world\create_message ModifyTestMessage, "entity", test_entity

        it "is called with correct arguments", ->
            world\__check_messages!
            world\__modify 0.01

    describe "state components", ->
        TestEntityMessage = EntityMessage\define "TestEntityMessage"
        TestStateMessageA = StateMessage\define "TestStateMessageA", value: "number"
        TestStateMessageB = StateMessage\define "TestStateMessageB", value: "number"

        class TestStateMessageEntityModifier extends EntityModifier
            @message_type: TestEntityMessage
            @state_message_types: { TestStateMessageA, TestStateMessageB }

            modify: =>

        world = World!
        modifier = world\add_modifier TestStateMessageEntityModifier

        it "is stored on the processor", ->
            test_state_message_a = world\create_message TestStateMessageA, "value", 2
            test_state_message_b = world\create_message TestStateMessageB, "value", 3
            world\__check_messages!
            world\__check_state_messages!
            assert.are.same test_state_message_a, modifier\get_state_message(TestStateMessageA)
            assert.are.same test_state_message_b, modifier\get_state_message(TestStateMessageB)
            assert.are.equal 2, test_state_message_a.value
            assert.are.equal 3, test_state_message_b.value

        it "is cleared after world processes message", ->
            test_state_message_a = world\create_message TestStateMessageA, "value", 2
            test_state_message_b = world\create_message TestStateMessageB, "value", 3
            world\__modify 0.01
            assert.is_not.has_value modifier.state_components, test_state_message_a
            assert.is_not.has_value modifier.state_components, test_state_message_b

    describe "modify not overridden", ->
        TestEntityMessage = EntityMessage\define "TestEntityMessage"

        func = ->
            class NotOverriddenModifier extends EntityModifier
                @message_type: TestEntityMessage

        it "throws an error", ->
            assert.has_error func, "NotOverriddenModifier must implement modify function"

    describe "defined with no message type", ->
        func = ->
            class NoMessageTypeModifier extends EntityModifier
                modify: =>

        it "throws an error", ->
            assert.has_error func, "NoMessageTypeModifier must have a message_type property"

    describe "defined with incorrect message type", ->
        TestComponentMessage = ComponentMessage\define "TestComponentMessage"

        func = ->
            class IncorrectMessageTypeModifier extends EntityModifier
                @message_type: TestComponentMessage
                modify: =>

        it "throws an error", ->
            assert.has_error func, "IncorrectMessageTypeModifier must consume a message of EntityMessage type"