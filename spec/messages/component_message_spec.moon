World = require "encompass.world"
Component = require "encompass.component"
ComponentMessage = require "encompass.messages.component"
MessagePool = require "encompass.message_pool"

helper = require "spec.helper"
helper.register assert

describe "MessageEntity", ->
    TestComponent = Component\define "TestComponent"
    TestComponentMessage = ComponentMessage\define "TestComponentMessage", component: TestComponent

    describe "when inheriting", ->
        InheritTestComponent = Component\define "TestComponent"
        InheritTestComponentMessage = ComponentMessage\define "TestComponentMessage", {component: InheritTestComponent, a: "number"}, {tag: "string"} 
        SubInheritTestComponentMessage = InheritTestComponentMessage\define "SubTestComponentMessage", {x: "number"}, {y: "string"}

        it "properly inherits and defines required field types", ->
            assert.are.same InheritTestComponent, SubInheritTestComponentMessage.required_field_types.component
            assert.are.same "number", SubInheritTestComponentMessage.required_field_types.x
            assert.are.same InheritTestComponent, SubInheritTestComponentMessage.field_types.component
            assert.are.same "number", SubInheritTestComponentMessage.field_types.x

        it "properly inherits and defines optional field types", ->
            assert.are.same "string", SubInheritTestComponentMessage.optional_field_types.tag
            assert.are.same "string", SubInheritTestComponentMessage.optional_field_types.y
            assert.are.same "string", SubInheritTestComponentMessage.field_types.tag
            assert.are.same "string", SubInheritTestComponentMessage.field_types.y

    describe "when checking subtype of", ->
        SubTestComponentMessage = TestComponentMessage\define "SubTestComponentMessage"

        describe "and it is a subtype", ->
            sub_test_component_message = SubTestComponentMessage!

            it "returns true", ->
                assert.true sub_test_component_message\subtype_of SubTestComponentMessage
                assert.true sub_test_component_message\subtype_of ComponentMessage

        describe "and it is not a subtype", ->
            SubComponentMessage = ComponentMessage\define "SubComponentMessage"
            sub_component_message = SubComponentMessage!

            it "returns false", ->
                assert.false sub_component_message\subtype_of SubTestComponentMessage

    describe "when deactivating", ->
        world = World!
        entity = world\create_entity!
        component = entity\add_component TestComponent
        message = world\create_message TestComponentMessage, "component", component
        message_pool = world.message_type_to_pool[TestComponentMessage]
        free = spy.on message_pool, "free"

        it "frees the message from its message pool", ->
            message\__deactivate!
            assert.spy(message_pool.free).was_called!
            assert.has_value message_pool.inactive_objects, message
            assert.not.has_value message_pool.active_objects, message
