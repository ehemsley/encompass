World = require "encompass.world"
Component = require "encompass.component"
SideEffectMessage = require "encompass.messages.side_effect"
MessagePool = require "encompass.message_pool"

helper = require "spec.helper"
helper.register assert

describe "MessageEntity", ->
    TestSideEffectMessage = SideEffectMessage\define "TestSideEffectMessage"

    describe "when inheriting", ->
        InheritTestSideEffectMessage = SideEffectMessage\define "TestSideEffectMessage", {a: "number"}, {tag: "string"} 
        SubInheritTestSideEffectMessage = InheritTestSideEffectMessage\define "SubTestSideEffectMessage", {x: "number"}, {y: "string"}

        it "properly inherits and defines required field types", ->
            assert.are.same InheritTestComponent, SubInheritTestSideEffectMessage.required_field_types.component
            assert.are.same "number", SubInheritTestSideEffectMessage.required_field_types.x
            assert.are.same InheritTestComponent, SubInheritTestSideEffectMessage.field_types.component
            assert.are.same "number", SubInheritTestSideEffectMessage.field_types.x

        it "properly inherits and defines optional field types", ->
            assert.are.same "string", SubInheritTestSideEffectMessage.optional_field_types.tag
            assert.are.same "string", SubInheritTestSideEffectMessage.optional_field_types.y
            assert.are.same "string", SubInheritTestSideEffectMessage.field_types.tag
            assert.are.same "string", SubInheritTestSideEffectMessage.field_types.y

    describe "when checking subtype of", ->
        SubTestSideEffectMessage = TestSideEffectMessage\define "SubTestSideEffectMessage"

        describe "and it is a subtype", ->
            sub_test_side_effect_message = SubTestSideEffectMessage!

            it "returns true", ->
                assert.true sub_test_side_effect_message\subtype_of SubTestSideEffectMessage
                assert.true sub_test_side_effect_message\subtype_of SideEffectMessage

        describe "and it is not a subtype", ->
            SubSideEffectMessage = SideEffectMessage\define "SubSideEffectMessage"
            sub_side_effect_message = SubSideEffectMessage!

            it "returns false", ->
                assert.false sub_side_effect_message\subtype_of SubTestSideEffectMessage

    describe "when deactivating", ->
        world = World!
        entity = world\create_entity!
        message = world\create_message TestSideEffectMessage
        message_pool = world.message_type_to_pool[TestSideEffectMessage]
        free = spy.on message_pool, "free"

        it "frees the message from its message pool", ->
            message\__deactivate!
            assert.spy(message_pool.free).was_called!
            assert.has_value message_pool.inactive_objects, message
            assert.not.has_value message_pool.active_objects, message
