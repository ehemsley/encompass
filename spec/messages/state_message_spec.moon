World = require "encompass.world"
Component = require "encompass.component"
StateMessage = require "encompass.messages.state"
MessagePool = require "encompass.message_pool"

helper = require "spec.helper"
helper.register assert

describe "MessageEntity", ->
    TestStateMessage = StateMessage\define "TestStateMessage"

    describe "when inheriting", ->
        InheritTestStateMessage = StateMessage\define "TestStateMessage", {a: "number"}, {tag: "string"} 
        SubInheritTestStateMessage = InheritTestStateMessage\define "SubTestStateMessage", {x: "number"}, {y: "string"}

        it "properly inherits and defines required field types", ->
            assert.are.same InheritTestComponent, SubInheritTestStateMessage.required_field_types.component
            assert.are.same "number", SubInheritTestStateMessage.required_field_types.x
            assert.are.same InheritTestComponent, SubInheritTestStateMessage.field_types.component
            assert.are.same "number", SubInheritTestStateMessage.field_types.x

        it "properly inherits and defines optional field types", ->
            assert.are.same "string", SubInheritTestStateMessage.optional_field_types.tag
            assert.are.same "string", SubInheritTestStateMessage.optional_field_types.y
            assert.are.same "string", SubInheritTestStateMessage.field_types.tag
            assert.are.same "string", SubInheritTestStateMessage.field_types.y

    describe "when checking subtype of", ->
        SubTestStateMessage = TestStateMessage\define "SubTestStateMessage"

        describe "and it is a subtype", ->
            sub_test_state_message = SubTestStateMessage!

            it "returns true", ->
                assert.true sub_test_state_message\subtype_of SubTestStateMessage
                assert.true sub_test_state_message\subtype_of StateMessage

        describe "and it is not a subtype", ->
            SubStateMessage = StateMessage\define "SubStateMessage"
            sub_state_message = SubStateMessage!

            it "returns false", ->
                assert.false sub_state_message\subtype_of SubTestStateMessage

    describe "when deactivating", ->
        world = World!
        entity = world\create_entity!
        message = world\create_message TestStateMessage
        message_pool = world.message_type_to_pool[TestStateMessage]
        free = spy.on message_pool, "free"

        it "frees the message from its message pool", ->
            message\__deactivate!
            assert.spy(message_pool.free).was_called!
            assert.has_value message_pool.inactive_objects, message
            assert.not.has_value message_pool.active_objects, message
